﻿using System;
using System.Collections.Generic;

namespace GCTProject.Data
{
    public partial class Tickets
    {
        public string PerformanceId { get; set; }
        public int SeatId { get; set; }

        public virtual Performance Performance { get; set; }
        public virtual Seats Seat { get; set; }
    }
}
