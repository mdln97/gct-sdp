﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GCTProject.Data
{
    public partial class Performance
    {
        public Performance()
        {
            BasketTickets = new HashSet<BasketTickets>();
            BookedSeats = new HashSet<BookedSeats>();
            Tickets = new HashSet<Tickets>();
        }
        [Key]
        public string Id { get; set; }
        public string PriceBand { get; set; }
        public DateTime LiveDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AgeRestriction { get; set; }
        public byte[] Picture { get; set; }

        public virtual ICollection<BasketTickets> BasketTickets { get; set; }
        public virtual ICollection<BookedSeats> BookedSeats { get; set; }
        public virtual ICollection<Tickets> Tickets { get; set; }
    }
}
