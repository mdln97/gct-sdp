﻿using System;
using System.Collections.Generic;

namespace GCTProject.Data
{
    public partial class BasketTickets
    {
        public string UserId { get; set; }
        public string PerformanceId { get; set; }
        public int SeatId { get; set; }
        public string ShippingMethod { get; set; }

        public virtual Performance Performance { get; set; }
        public virtual Seats Seat { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
