﻿using System;
using System.Collections.Generic;

namespace GCTProject.Data
{
    public partial class BookedSeats
    {
        public string PerformanceId { get; set; }
        public int Seatid { get; set; }
        public bool? Booked { get; set; }

        public virtual Performance Performance { get; set; }
        public virtual Seats Seat { get; set; }
    }
}
