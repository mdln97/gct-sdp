﻿using System;
using System.Collections.Generic;

namespace GCTProject.Data
{
    public partial class Seats
    {
        public Seats()
        {
            BasketTickets = new HashSet<BasketTickets>();
            BookedSeats = new HashSet<BookedSeats>();
            Tickets = new HashSet<Tickets>();
        }

        public int Id { get; set; }
        public string RowName { get; set; }
        public string ColumnNumber { get; set; }

        public virtual ICollection<BasketTickets> BasketTickets { get; set; }
        public virtual ICollection<BookedSeats> BookedSeats { get; set; }
        public virtual ICollection<Tickets> Tickets { get; set; }
    }
}
