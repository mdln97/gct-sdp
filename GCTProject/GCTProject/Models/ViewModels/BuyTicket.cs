﻿using GCTProject.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GCTProject.Models.ViewModels
{
    public class BuyTicket
    {
        public string PerformanceId { get; set; }
        public int SeatId { get; set; }
        public List<string> rowNames { get; set; }
        public List<decimal> Costs { get; set; }
        public int ManySeats { get; set; }
        public DateTime LiveDate { get; set; }
        public string PerformanceName { get; set; }
        public List<BookedSeats> BookedSeats { get; set; }
        public List<Seats> Seats { get; set; }
        public SelectList rowTags { get; set; }
        public SelectList columnsTags { get; set; }
        public List<string> chosenRows { get; set; }
        public List<string> chosenColumns { get; set; }

    }
}
