﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GCTProject.Data;
using GCTProject.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Stripe;

namespace GCTProject.Controllers
{
    public class BookingController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _contextId;
        private readonly GCTProjectContext _contextData;

        public BookingController(
                    UserManager<IdentityUser> userManager,
                    ApplicationDbContext contextId,
                    GCTProjectContext context)
        {
            _userManager = userManager;
            _contextId = contextId;
            _contextData = context;
        }

        //GET Basket/Booking
        public async Task<IActionResult> Basket()
        {
            var user = await _userManager.GetUserAsync(User);
            var basketTickets = from bt in _contextData.BasketTickets
                                select bt;
            basketTickets = basketTickets.Where(bt => bt.UserId == user.Id);

            if (basketTickets != null)
            {

                var savedRepresentations = new List<TicketsInBasket>();
                
                foreach (var item in basketTickets)
                {
                    var tb = new TicketsInBasket();
                    tb.Id = item.UserId;
                    tb.Cost = Int32.Parse(item.Performance.PriceBand.Split('-')[0]);
                    tb.RowNumber = item.Seat.RowName;
                    tb.SeatNumber = item.Seat.ColumnNumber;
                    tb.PerformanceName = item.Performance.Name;
                    tb.PerformanceTime = item.Performance.LiveDate;
                    savedRepresentations.Add(tb);
                }

                return View(savedRepresentations);
            }

            return View();
        }

        //GET Buy/Booking
        public async Task<IActionResult> Buy(string Id = null)
        {
            if(Id == null)
            {
                return NotFound();
            }

            var performance = await _contextData.Performance.FindAsync(Id);
            var seatsBooked =  _contextData.BookedSeats.Where(s => s.PerformanceId == performance.Id);
            IQueryable<string> rows = from m in _contextData.Seats
                                      orderby m.RowName
                                      select m.RowName;
            IQueryable<string> column = from m in _contextData.Seats
                                      orderby m.ColumnNumber
                                      select m.ColumnNumber;
            List<string> columns1, rows1;
             rows1 = new List<string>();
            columns1 = new List<string>();
            for (int j=0; j<24; j++)
            {
                columns1.Add("Select");
                rows1.Add("Select");
            }
            string[] splitPrices = performance.PriceBand.Split("-");
            decimal startPrice = decimal.Parse(splitPrices[0]);
            decimal endPrice = decimal.Parse(splitPrices[1]);
            List<decimal> costs = new List<decimal>();
            int rowsNumber = rows.Distinct().Count();
            costs.Add(decimal.Parse(endPrice.ToString("0.00")));
            for (int i = 2; i < rowsNumber; i++)
            {
                costs.Add(decimal.Parse((endPrice - (endPrice - startPrice)/(i)).ToString("0.00")));
            }
            costs.Add(decimal.Parse(startPrice.ToString("0.00")));

            BuyTicket buyTicket = new BuyTicket() { PerformanceId = performance.Id, PerformanceName = performance.Name, rowNames = rows.Distinct().ToList(), Costs = costs, LiveDate = performance.LiveDate,
                BookedSeats = seatsBooked.ToList(), Seats = _contextData.Seats.ToList(),
                rowTags = new SelectList(rows.Distinct().ToList()), columnsTags = new SelectList(column.Distinct().ToList()),
                chosenColumns =columns1, chosenRows = rows1
        };
            ViewData["rows"] = rowsNumber;
            ViewData["columns"] = column.Distinct().Count();
            
            return View(buyTicket);
        }

        //GET Charge/Booking
        public IActionResult Charge(string stripeEmail, string stripeToken)
        {
            var customers = new CustomerService();
            var charges = new ChargeService();

            var customer = customers.Create(new CustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new ChargeCreateOptions
            {
                Amount = 500,
                Description = "Initial Charge",
                Currency = "gbp",
                CustomerId = customer.Id
            });

            return View();
        }
    }
}